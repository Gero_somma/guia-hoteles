$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 4000
    });

    $('#contacto').on('show.bs.modal', function (e){
        console.log('Se esta mostrando');

        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-primary');

        $('#contactoBtn').prop('disabled', true);
    });

    $('#contacto').on('shown.bs.modal', function (e){
        console.log('Termina de mostrarse');
    });

    $('#contacto').on('hide.bs.modal', function (e){
        console.log('Se va a cerrar');
    });

    $('#contacto').on('hidden.bs.modal', function (e){
        console.log('Termina de ocultarse');
        $('#contactoBtn').prop('disabled', false);
    });
});